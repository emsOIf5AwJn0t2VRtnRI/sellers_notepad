# sellers_notepad

A small notes web app with two basic fields: a client name and notes. This app is primarily thought to be used by individuals with small/medium size businesses in which they have requests/orders from clients to be fulfilled. 

# dependencies
django >= 3.2